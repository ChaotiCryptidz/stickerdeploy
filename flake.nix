{
  description = "A tool for deploying sticker packs";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/release-22.05";
    utils.url = "github:numtide/flake-utils";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, utils, ... }:
    {
      overlay = final: prev:
        let system = final.system;
        in {
          stickerdeploy = final.rustPlatform.buildRustPackage rec {
            pname = "stickerdeploy";
            version = "latest";

            src = ./.;
            cargoLock = { lockFile = ./Cargo.lock; };

            doCheck = false;
            nativeBuildInputs = with final.pkgs; [ pkg-config rustc cargo ];
          };
        };
    } // utils.lib.eachSystem (utils.lib.defaultSystems) (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ self.overlay ];
        };
      in {
        defaultPackage = self.packages."${system}".stickerdeploy;
        packages.stickerdeploy = pkgs.stickerdeploy;

        apps = rec {
          stickerdeploy = {
            type = "app";
            program = "${self.defaultPackage.${system}}/bin/stickerdeploy";
          };
          default = stickerdeploy;
        };

        defaultApp = self.apps."${system}".stickerdeploy;

        devShell = pkgs.mkShell {
          RUST_SRC_PATH = pkgs.rustPlatform.rustLibSrc;
          buildInputs = with pkgs; [ rustc cargo rust-analyzer rustfmt clippy ];
        };

        lib = pkgs.stickerdeploy.lib;
      });
}
