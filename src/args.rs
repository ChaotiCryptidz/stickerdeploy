use clap::{Args, Parser, Subcommand};

#[derive(Debug, Parser)]
#[clap()]
pub struct CLIArgs {
    #[clap(long)]
    pub debug: bool,
    #[clap(subcommand)]
    pub command: Commands,
}

#[derive(Debug, Subcommand)]
pub enum Commands {
    Deploy(DeployArgs),
    CreateTelegramStickerPack(CreateTelegramStickerPackArgs),
    CheckMissing(CheckMissingArgs),
}

#[derive(Debug, Args)]
pub struct DeployArgs {
    pub folder: String,
    pub deploy_ids: Vec<String>,
}

#[derive(Debug, Args)]
pub struct CreateTelegramStickerPackArgs {
    pub token: String,
    pub user_id: u64,
    pub name: String,
    pub title: String,
    pub emojis: String,
    pub r#type: String,
    pub filename: String,
}

#[derive(Debug, Args)]
pub struct CheckMissingArgs {
    pub folder: String,
    pub sticker_sets: Vec<String>,
}
