use std::{collections::HashMap, fs::File, path::PathBuf};

use crate::{args::CheckMissingArgs, sticker_config::StickerConfig};

pub async fn check_missing(args: CheckMissingArgs) {
    let base_path = PathBuf::from(&args.folder);
    let config_path = base_path.join("stickers.yml");
    let config_file = File::open(config_path).expect("could not open stickers.yml");
    let config: StickerConfig =
        serde_yaml::from_reader(config_file).expect("could not parse stickers.yml");

    let stickers_for_sticker_sets: Vec<(&String, &Vec<String>)> = args
        .sticker_sets
        .iter()
        .map(|pack_id| {
            println!("{}", pack_id);
            return (pack_id, config.sticker_sets.get(pack_id).unwrap());
        })
        .collect();

    let missing_stickers: Vec<(&String, Vec<&String>)> = stickers_for_sticker_sets
        .iter()
        .map(|(sticker_set_name, sticker_set_stickers)| {
            return (
                *sticker_set_name,
                config
                    .stickers
                    .iter()
                    .filter(|all_stickers_sticker| {
                        !sticker_set_stickers.contains(all_stickers_sticker.0)
                    })
                    .map(|b| b.0)
                    .collect::<Vec<&String>>(),
            );
        })
        .collect();

    let mut total_missing: Vec<&String> = HashMap::<&String, bool>::from_iter(
        missing_stickers
            .iter()
            .map(|(_pack_name, stickers)| stickers.clone())
            .collect::<Vec<Vec<&String>>>()
            .concat()
            .iter()
            .map(|sticker_name| (*sticker_name, true)),
    )
    .into_keys()
    .collect();
    total_missing.sort();

    let mut missing_from_all: Vec<&String> = total_missing
        .clone()
        .into_iter()
        .filter(|missing_sticker| {
            return !missing_stickers
                .iter()
                .map(|(_pack_name, stickers)| stickers.contains(missing_sticker))
                .any(|x| !x);
        })
        .collect();
    missing_from_all.sort();

    for (pack_name, stickers) in missing_stickers.clone().iter_mut() {
        stickers.sort();
        println!("Pack: {}\nMissing: {:#?}\n", pack_name, stickers);
    }

    println!("Total Missing {:#?}\n", total_missing);

    println!("Missing From All {:#?}", missing_from_all);
}
