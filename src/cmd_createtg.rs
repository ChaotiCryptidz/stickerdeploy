use crate::args::CreateTelegramStickerPackArgs;
use crate::tg_api::TelegramAPI;

use std::fs::File;
use std::io::Read;

pub async fn create_telegram_sticker_pack(args: CreateTelegramStickerPackArgs) {
    let bot = TelegramAPI::new(args.token);

    let mut file = File::open(args.filename).unwrap();
    let mut file_data: Vec<u8> = Vec::new();
    file.read_to_end(&mut file_data)
        .expect("could not read file");

    let mut png_sticker: Option<Vec<u8>> = None;
    let mut tgs_sticker: Option<Vec<u8>> = None;
    let mut webm_sticker: Option<Vec<u8>> = None;

    match args.r#type.as_str() {
        "regular" => {
            png_sticker = Some(file_data);
        }
        "animated" => {
            tgs_sticker = Some(file_data);
        }
        "video" => {
            webm_sticker = Some(file_data);
        }
        _ => {
            panic!("wrong type, use regular, animated or video")
        }
    }

    bot.create_sticker_pack(
        args.user_id,
        args.name,
        args.title,
        args.emojis,
        png_sticker,
        tgs_sticker,
        webm_sticker,
    )
    .await
    .expect("could not create sticker pack");
}
