use crate::deploy_discord::deploy_discord;
use crate::deploy_mastodon::deploy_mastodon;
use crate::deploy_telegram::deploy_telegram;
use crate::deploy_zip::deploy_zip;
use crate::sticker_config::StickerConfig;

use std::collections::HashMap;
use std::fs::File;
use std::path::PathBuf;

pub async fn deploy(args: crate::args::DeployArgs) {
    let base_path = PathBuf::from(&args.folder);
    let config_path = base_path.join("stickers.yml");
    let creds_path = base_path.join("../stickerdb/creds.yml");

    let config_file = File::open(config_path).expect("could not open stickers.yml");
    let creds_file = File::open(creds_path).expect("could not open creds.yml");

    let config: StickerConfig =
        serde_yaml::from_reader(config_file).expect("could not parse stickers.yml");
    let creds: HashMap<String, String> =
        serde_yaml::from_reader(creds_file).expect("could not parse creds.yml");

    let mut deploy_ids = args.deploy_ids;

    if deploy_ids.is_empty() {
        deploy_ids = config.deploy_where.keys().cloned().collect::<Vec<String>>();
    }

    for deploy_id in deploy_ids.into_iter() {
        let sticker_config = config.clone();

        match sticker_config
            .deploy_where
            .get(&deploy_id)
            .expect("no deploy config with id specified found")
            .deploy_to
            .as_str()
        {
            "discord" => {
                println!("deploying {} to discord", &deploy_id);
                deploy_discord(
                    deploy_id,
                    sticker_config,
                    creds.clone(),
                    args.folder.clone(),
                )
                .await;
            }
            "zip" => {
                println!("deploying {} to zip", &deploy_id);
                deploy_zip(
                    deploy_id,
                    sticker_config,
                    args.folder.clone(),
                )
                .await;
            }
            "telegram" => {
                println!("deploying {} to telegram", &deploy_id);
                deploy_telegram(
                    deploy_id,
                    sticker_config,
                    creds.clone(),
                    args.folder.clone(),
                )
                .await;
            }
            "mastodon" => {
                println!("deploying {} to mastodon", &deploy_id);
                deploy_mastodon(
                    deploy_id,
                    sticker_config,
                    creds.clone(),
                    args.folder.clone(),
                )
                .await;
            }
            _ => {
                panic!("deploy_to not set")
            }
        }
    }
}
