use std::{collections::HashMap, io::Read, path::PathBuf};

use crate::{
    mastodon_api::{AdminEmoji, MastodonAPI},
    sticker_config::{Sticker, StickerConfig}, utils::convert_to_png,
};

pub async fn deploy_mastodon(
    deploy_id: String,
    sticker_config: StickerConfig,
    creds: HashMap<String, String>,
    base_stickerdb_path: String,
) {
    let deploy_where = sticker_config.deploy_where.get(&deploy_id).unwrap();

    let deploy_location = deploy_where.mastodon.as_ref().unwrap();

    let pack_contents = sticker_config
        .sticker_sets
        .get(&deploy_where.pack_id)
        .unwrap();

    let pack_stickers: HashMap<String, Sticker> = pack_contents
        .iter()
        .map(|sticker_name| {
            return (
                sticker_name.clone(),
                sticker_config.stickers.get(sticker_name).unwrap().clone(),
            );
        })
        .collect();

    let token = creds.get(&deploy_location.credential_name).unwrap();

    let mastodon_api = MastodonAPI::new(token.to_string(), deploy_location.api_base.clone());

    let filter = vec!["domain:local".to_string()];

    let mut mastodon_emojis = mastodon_api
        .get_custom_emoji(&filter)
        .await
        .expect("could not get custom emojis");

    // Delete emojis which have been removed from list
    {
        let invalid_emojis: Vec<AdminEmoji> = mastodon_emojis
            .clone()
            .into_iter()
            .filter(|emoji| {
                !(pack_contents.contains(&emoji.shortcode)
                    || emoji.category != deploy_location.category)
            })
            .collect();

        for invalid_emoji in invalid_emojis.iter() {
            println!("Deleting emoji with name {}", &invalid_emoji.shortcode);
            mastodon_api
                .delete_custom_emoji(&invalid_emoji.id)
                .await
                .expect("could not delete emoji");
        }
    }

    if let Some(category) = &deploy_location.category {
        mastodon_emojis = mastodon_emojis
            .into_iter()
            .filter(|emoji| emoji.category.is_some())
            .filter(|emoji| emoji.category == Some(category.to_string()))
            .collect();
    } else {
        mastodon_emojis = mastodon_emojis
            .into_iter()
            .filter(|emoji| emoji.category.is_none())
            .collect()
    }

    // add emoji that arent in mastodon_emojis
    {
        let missing_emoji: Vec<String> = pack_contents
            .clone()
            .into_iter()
            .filter(|emoji_name| {
                for emoji in mastodon_emojis.iter() {
                    if &emoji.shortcode == emoji_name {
                        return false;
                    }
                }
                true
            })
            .collect();

        for emoji_name in missing_emoji.iter() {
            println!("Uploading Emoji: {}", emoji_name);
            let sticker_data = pack_stickers.get(emoji_name).unwrap();

            let file_path = PathBuf::from(&base_stickerdb_path).join(&sticker_data.file);

            let mut file_data: Vec<u8>;
            if file_path.extension().unwrap() != "png" {
                file_data = convert_to_png(file_path);
            } else {
                file_data = Vec::new();

                let mut file = std::fs::File::open(file_path).unwrap();
                file.read_to_end(&mut file_data)
                    .expect("could not read file");
            }

            mastodon_api
                .upload_emoji(
                    emoji_name.clone(),
                    deploy_location.category.clone(),
                    file_data,
                )
                .await
                .expect("could not upload emoji");
        }
    }
}
