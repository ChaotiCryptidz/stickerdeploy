use std::{
    collections::HashMap,
    fs::File,
    io::{Read, Write},
    path::{Path, PathBuf},
};

use crate::{
    sticker_config::{Sticker, StickerConfig},
    utils::convert_to_png,
};

pub async fn deploy_zip(
    deploy_id: String,
    sticker_config: StickerConfig,
    base_stickerdb_path: String,
) {
    let deploy_where = sticker_config.deploy_where.get(&deploy_id).unwrap();

    let should_convert_png = deploy_where.zip.as_ref().unwrap().convert_png;

    let pack_contents = sticker_config
        .sticker_sets
        .get(&deploy_where.pack_id)
        .unwrap();

    let pack_stickers: HashMap<String, Sticker> = pack_contents
        .iter()
        .map(|sticker_name| {
            return (
                sticker_name.clone(),
                sticker_config.stickers.get(sticker_name).unwrap().clone(),
            );
        })
        .collect();

    let zip_path = format!("{}.zip", &deploy_id).to_string();
    let path = Path::new(&zip_path);
    let file = File::create(path).unwrap();

    let mut zip = zip::ZipWriter::new(file);
    let mut buffer = Vec::new();
    for (name, sticker) in pack_stickers.into_iter() {
        let sticker_path = PathBuf::from(&base_stickerdb_path).join(sticker.file);

        let sticker_extension = match should_convert_png {
            false => (&sticker_path)
                .extension()
                .unwrap()
                .to_string_lossy()
                .to_string(),
            true => "png".to_string(),
        };

        zip.start_file(
            format!("{}.{}", name, sticker_extension),
            Default::default(),
        )
        .unwrap();
        if should_convert_png {
            let file_data = convert_to_png(sticker_path);
            buffer.write_all(&file_data);
        } else {
            let mut sticker_file = File::open(&sticker_path).unwrap();
            sticker_file.read_to_end(&mut buffer).unwrap();
        }
        zip.write_all(&buffer).unwrap();
        buffer.clear();
    }
    zip.finish().unwrap();
}
