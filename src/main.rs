pub mod args;
pub mod cmd_check_missing;
pub mod cmd_createtg;
pub mod cmd_deploy;
pub mod deploy_discord;
pub mod deploy_mastodon;
pub mod deploy_telegram;
pub mod deploy_zip;
pub mod mastodon_api;
pub mod sticker_config;
pub mod tg_api;
pub mod utils;

use clap::Parser;

use args::{CLIArgs, Commands};
use cmd_check_missing::check_missing;
use cmd_createtg::create_telegram_sticker_pack;
use cmd_deploy::deploy;

#[tokio::main]
async fn main() {
    let args = CLIArgs::parse();

    if args.debug {
        let subscriber = tracing_subscriber::fmt()
            .with_max_level(tracing::Level::DEBUG)
            .pretty()
            .finish();

        tracing::subscriber::set_global_default(subscriber)
            .expect("setting tracing default failed");
    }

    match args.command {
        Commands::Deploy(subcommand_args) => {
            deploy(subcommand_args).await;
        }
        Commands::CreateTelegramStickerPack(subcommand_args) => {
            create_telegram_sticker_pack(subcommand_args).await;
        }
        Commands::CheckMissing(subcommand_args) => {
            check_missing(subcommand_args).await;
        }
    }
}
