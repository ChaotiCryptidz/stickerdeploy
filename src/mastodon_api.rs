use reqwest::multipart::Part;
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, error::Error};

// API docs taken from https://github.com/superseriousbusiness/gotosocial/blob/main/docs/api/swagger.yaml

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MastodonErrorResp {
    pub error: String,
}

impl MastodonErrorResp {
    fn to_error(&self, error_code: u16) -> MastodonAPIError {
        MastodonAPIError::new(self.error.clone(), error_code)
    }
}

#[derive(Debug)]
struct MastodonAPIError {
    error: String,
    error_code: u16,
}

impl MastodonAPIError {
    fn new(error: String, error_code: u16) -> MastodonAPIError {
        MastodonAPIError { error, error_code }
    }
}

impl std::fmt::Display for MastodonAPIError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}: {}", self.error_code, self.error)
    }
}

impl Error for MastodonAPIError {
    fn description(&self) -> &str {
        self.error.as_str()
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AdminEmoji {
    pub id: String,
    pub category: Option<String>,
    pub shortcode: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Emoji {
    pub category: Option<String>,
    pub shortcode: String,
}

pub struct MastodonAPI {
    token: String,
    api_base: String,
    client: reqwest::Client,
}

impl MastodonAPI {
    pub fn new(token: String, api_base: String) -> Self {
        Self {
            token,
            api_base,
            client: reqwest::Client::builder()
                .user_agent("stickerdeploy")
                .build()
                .unwrap(),
        }
    }

    pub async fn get_custom_emoji(
        &self,
        filter: &[String],
    ) -> Result<Vec<AdminEmoji>, Box<dyn Error>> {
        let url = format!("{}/api/v1/admin/custom_emojis", self.api_base);

        let mut query: HashMap<String, String> = HashMap::new();
        query.insert("filter".to_string(), filter.join(","));
        query.insert("limit".to_string(), "0".to_string());

        let res = self
            .client
            .get(url)
            .header(
                "Authorization".to_string(),
                format!("Bearer {}", self.token).to_string(),
            )
            .query(&query)
            .send()
            .await?;

        let status = res.status();
        if status.is_success() {
            let json = res.json::<Vec<AdminEmoji>>().await?;

            Ok(json)
        } else {
            let json = res.json::<MastodonErrorResp>().await?;

            Err(Box::new(json.to_error(status.as_u16())))
        }
    }

    pub async fn delete_custom_emoji(&self, id: &String) -> Result<(), Box<dyn Error>> {
        let url = format!("{}/api/v1/admin/custom_emojis/{}", self.api_base, id);

        let res = self
            .client
            .delete(url)
            .header(
                "Authorization".to_string(),
                format!("Bearer {}", self.token).to_string(),
            )
            .send()
            .await?;

        let status = res.status();
        if !status.is_success() {
            let json = res.json::<MastodonErrorResp>().await?;

            Err(Box::new(json.to_error(status.as_u16())))
        } else {
            Ok(())
        }
    }

    pub async fn upload_emoji(
        &self,
        shortcode: String,
        category: Option<String>,
        file_data: Vec<u8>,
    ) -> Result<Emoji, Box<dyn Error>> {
        let url = format!("{}/api/v1/admin/custom_emojis", self.api_base);

        let mut form = reqwest::multipart::Form::new();
        form = form.text("shortcode", shortcode.clone());
        if let Some(category) = category {
            form = form.text("category", category);
        } else {
            form = form.text("category", "");
        }
        form = form.part("image", Part::bytes(file_data).file_name("test.png"));

        let res = self
            .client
            .post(url)
            .header(
                "Authorization".to_string(),
                format!("Bearer {}", self.token).to_string(),
            )
            .multipart(form)
            .send()
            .await?;

        let status = res.status();
        if status.is_success() {
            let json = res.json::<Emoji>().await?;

            Ok(json)
        } else {
            let json = res.json::<MastodonErrorResp>().await?;

            Err(Box::new(json.to_error(status.as_u16())))
        }
    }
}
