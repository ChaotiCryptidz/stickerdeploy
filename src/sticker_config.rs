use serde::Deserialize;
use std::collections::HashMap;

#[derive(Debug, Clone, Deserialize)]
pub struct StickerConfig {
    pub stickers: HashMap<String, Sticker>,
    pub credits: HashMap<String, Credit>,
    pub deploy_where: HashMap<String, DeployWhere>,
    pub sticker_sets: HashMap<String, Vec<String>>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct DeployWhere {
    pub pack_id: String,
    pub deploy_to: String,
    pub discord: Option<Vec<DiscordDeployLocation>>,
    pub telegram: Option<TelegramDeployLocation>,
    pub mastodon: Option<MastodonDeployLocation>,
    pub zip: Option<ZipDeployLocation>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct TelegramDeployLocation {
    #[serde(default = "telegram_credential_name_default")]
    pub credential_name: String,
    pub r#type: StickerType,
    pub name: String,
    pub user_id: u64,
}

fn telegram_credential_name_default() -> String {
    "telegram".to_string()
}

#[derive(Debug, Clone, Deserialize)]
pub struct MastodonDeployLocation {
    pub api_base: String,
    #[serde(default = "mastodon_credential_name_default")]
    pub credential_name: String,
    pub category: Option<String>,
}

fn mastodon_credential_name_default() -> String {
    "mastodon".to_string()
}

#[derive(Debug, Clone, Deserialize)]
pub struct ZipDeployLocation {
    pub convert_png: bool,
}

#[derive(Debug, Clone, Deserialize)]
pub struct DiscordDeployLocation {
    pub deploy_name: String,
    pub id: u64,
    #[serde(default = "discord_credential_name_default")]
    pub credential_name: String,
    #[serde(default = "discord_max_regular_emoji_default")]
    pub max_regular_emoji: u64,
    #[serde(default = "discord_max_animated_emoji_default")]
    pub max_animated_emoji: u64,
}

fn discord_credential_name_default() -> String {
    "discord".to_string()
}

fn discord_max_regular_emoji_default() -> u64 {
    50
}

fn discord_max_animated_emoji_default() -> u64 {
    50
}

#[derive(Debug, Clone, Deserialize)]
pub struct Credit {
    pub artist: String,
    pub source: Vec<String>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Sticker {
    pub file: String,
    pub emojis: Vec<String>,
    pub alt_text: Option<String>,
    pub credit: Option<String>,
    #[serde(default)]
    pub r#type: StickerType,
}

#[derive(Debug, PartialEq, Eq, Clone, serde_with::DeserializeFromStr)]
pub enum StickerType {
    Regular,
    Gif,
    TelegramVideo,
    TelegramAnimated,
}

impl std::str::FromStr for StickerType {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "regular" => Ok(Self::Regular),
            "gif" => Ok(Self::Gif),
            "telegram_video" => Ok(Self::TelegramVideo),
            "telegram_animated" => Ok(Self::TelegramAnimated),
            _ => Err(String::from("invalid sticker type")),
        }
    }
}

impl std::fmt::Display for StickerType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match *self {
            StickerType::Regular => write!(f, "regular"),
            StickerType::Gif => write!(f, "gif"),
            StickerType::TelegramVideo => write!(f, "telegram_video"),
            StickerType::TelegramAnimated => write!(f, "telegram_animated"),
        }
    }
}

impl Default for StickerType {
    fn default() -> Self {
        StickerType::Regular
    }
}
