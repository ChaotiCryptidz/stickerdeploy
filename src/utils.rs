use std::{path::PathBuf, process::Command};

pub fn convert_to_png(path: PathBuf) -> Vec<u8> {
  let output = Command::new("convert")
      .args([
          "-format",
          "png",
          "-resize",
          "128x128",
          path.as_os_str().to_str().unwrap(),
          "png:-",
      ])
      .output()
      .expect("failed to execute process");

  if !output.status.success() {
      panic!(
          "failed to run convert, stderr: {}",
          String::from_utf8_lossy(&output.stderr)
      );
  }

  output.stdout
}